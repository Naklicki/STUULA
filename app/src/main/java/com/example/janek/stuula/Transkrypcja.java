package com.example.janek.stuula;

        import android.app.ActionBar;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TextView;

public class Transkrypcja extends AppCompatActivity {

    private Button wynik;
    private EditText dane;
    private String RNA = "";
    private TextView tvRNA;
    private String output = "";
    private Button clear;
    private String empty = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transkrypcja);


        wynik = findViewById(R.id.btnWynik);
        tvRNA = findViewById(R.id.tvWnik);
        dane = findViewById(R.id.etDNA);
        clear = findViewById(R.id.btnClear);


        wynik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RNA = dane.getText().toString();


                for (int i = 0; i < RNA.length(); i++)
                {

                    if (RNA.charAt(i) == 'A')                                                   // = A
                    {
                        output = output + "U";
                    }
                    if (RNA.charAt(i) == 'G')                                                   // = G
                    {
                        output = output + "C";
                    }
                    if (RNA.charAt(i) == 'T')                                                   // = G
                    {
                        output = output + "A";
                    }
                    if (RNA.charAt(i) == 'U')                                                   // = U
                    {
                        output = output + 'A';
                    }
                    if (RNA.charAt(i) == 'C')                                                   // = C
                    {
                        output = output + 'G';
                    }
                    if (RNA.charAt(i) == ' ')
                    {
                        output = output +' ';
                    }

                }

                tvRNA.setText(output);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                output = empty;
                tvRNA.setText(empty);
            }
        });



    }
}
