package com.example.janek.stuula;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SpisKodonow extends AppCompatActivity {


    String[] Kodony = {"AUG", "GGG"};
    String[] Descriptions = {"metiona", "glicyna"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spiskodonow);

        ListView listView = (ListView)findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();

        listView.setAdapter(customAdapter);

    }
    class  CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return Kodony.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertview, ViewGroup parent) {
            convertview = getLayoutInflater().inflate(R.layout.customlayout, null);

            TextView textView_name = (TextView)findViewById(R.id.tvName);
            TextView textView_description = (TextView)findViewById(R.id.tvDescription);

            textView_name.setText(Kodony[position]);
            textView_description.setText(Descriptions[position]);


            return convertview;
        }
    }
}
