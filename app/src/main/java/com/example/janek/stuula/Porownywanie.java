package com.example.janek.stuula;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Porownywanie extends AppCompatActivity {

    private TextView pasta;
    private int click = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nie_naciskaj);

        pasta = findViewById(R.id.pasta);


        pasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                click++;
                if (click == 10)
                {
                    pasta.setText("Naprawdę nic tu nie ma");
                }
                else if (click == 20)
                {
                    pasta.setText("Czy ja czegoś nie powiedziałem?!");
                }
                else if (click == 30)
                {
                    pasta.setText("Pff...");
                }
                else if (click == 35)
                {
                    pasta.setText("Zaraz się doigrasz!");
                }
                else if (click > 40)
                {
                    startActivity(new Intent(Porownywanie.this, Story.class));
                }
            }
        });


    }
}
