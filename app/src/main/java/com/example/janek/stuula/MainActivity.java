package com.example.janek.stuula;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button Translacja;
    private Button Transkrypcja;
    private Button Wlasciwosci;
    private Button Porownywanie;
    private TextView Logo;
    private int click = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Logo = findViewById(R.id.STUULA);
        Transkrypcja = findViewById(R.id.btnTrasnskrypcja);
        Translacja = findViewById(R.id.btnTranslacja);
        Porownywanie = findViewById(R.id.btnEE);
        Wlasciwosci = findViewById(R.id.btnWlasc);

        Transkrypcja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Transkrypcja.class));
            }
        });

        Translacja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Translacja.class));
            }
        });

        Porownywanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MainActivity.class));
            }
        });

        Wlasciwosci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MainActivity.class));
            }
        });
        Logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click++;
                if (click == 10)
                {
                    Logo.setText("CLICK!");
                }
                if (click == 15)
                {
                    startActivity(new Intent(MainActivity.this, Story.class));
                }
            }
        });





    }
}
