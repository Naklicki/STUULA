package com.example.janek.stuula;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class TranslacjaTEST extends AppCompatActivity {

    // STRING
    private String space = "{SPACE}";
    private String kodonList = "";
    private String podanyKodon = "";
    private String wynikOstat = "";
    private String wynik = "";
    // INT
    private int licznik = 0;
    public int a = 0;
    // BUTTON
    private Button add;
    private Button test;
    private Button clear;

    private Button rez;
    // EDIT TEXT
    private EditText etKodon;
    //Text View
    private TextView tvWynik;
    //LIST
    public final List<String> lista = new ArrayList<>();  //   STRING LIST


    //TEST
    private Button A;
    private int id;
    private Button start;
    private Button end;
    private String kodon = "";





    final Pattern pAlpha = Pattern.compile("[A-U-C-G]+");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translacjatest);



            add = findViewById(R.id.btnADD);               // FIND BTN ADD
            rez  = findViewById(R.id.btnRez);              // FIND BTN END
            etKodon = findViewById(R.id.etTranslacja);     // FIND EDIT TEXT TRANSLACJA
            tvWynik = findViewById(R.id.tvWnikTranslacja); // FIND TEXT VIEW WYNIK TRANSLACJI
            clear = findViewById(R.id.btnClear);             // FIND BTN BACK
            A = findViewById(R.id.A);
            start = findViewById(R.id.Start);
            end =  findViewById(R.id.end);

            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchOption();
                }
            });
            end.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etKodon.setText(kodonList);
                }
            });


                //                                                  ADD BUTTON
            add.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {
                    add();
                }
            });


        //                                                          END BUTTON
            rez.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v)
                {
                   end();
                }

                 });
        }
            // ------------------------------------------------- END OF ON CREATE  ------------------------------------










            //                                                  STRING CONVERTER IF CONSTRUCTION MAIN CODE
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void translIf(List<String> lista, int licznik)
    {
        for ( int i = a ; i < licznik; i++)
        {
            if (lista.get(i).equals(""))
            {

            }
            if (lista.get(i).equals("AUG"))
            {
                wynik = wynik + "Metionina ";

            }
            if (lista.get(i).equals("UUU") || lista.get(i).equals("UUC"))
            {
                wynik = wynik + "fenyloalanina ";
            }
            if (lista.get(i).equals("UUA") || lista.get(i).equals("UUG") || lista.get(i).equals("CUU") || lista.get(i).equals("CUC") || lista.get(i).equals("CUA") || lista.get(i).equals("CUG"))
            {
                wynik = wynik + "leucyna ";
            }
            if (lista.get(i).equals("AUU") || lista.get(i).equals("AUA") || lista.get(i).equals("AUC"))
            {
                wynik = wynik + "izoleucyna ";
            }
            if (lista.get(i).equals("GUU") || lista.get(i).equals("GUC") || lista.get(i).equals("GUA") || lista.get(i).equals("GUG"))
            {
                wynik = wynik + "walina ";
            }
            if (lista.get(i).equals("UCU") || lista.get(i).equals("UCC") || lista.get(i).equals("UCA") || lista.get(i).equals("UCG") || lista.get(i).equals("AGU") || lista.get(i).equals("AGC"))
            {
                wynik = wynik + "seryna ";
            }
            if (lista.get(i).equals("CCU") || lista.get(i).equals("CCC") || lista.get(i).equals("CCA") || lista.get(i).equals("CCG"))
            {
                wynik = wynik + "prolina ";
            }
            if (lista.get(i).equals("ACU") || lista.get(i).equals("ACC") || lista.get(i).equals("ACA") || lista.get(i).equals("ACG"))
            {
                wynik = wynik + "treonina ";
            }
            if (lista.get(i).equals("GCU") || lista.get(i).equals("GCC") || lista.get(i).equals("GCA") || lista.get(i).equals("GCG"))
            {
                wynik = wynik + "alanina ";
            }
            if (lista.get(i).equals("UAU") || lista.get(i).equals("UAC"))
            {
                wynik = wynik + "tyrozyna ";
            }
            if (lista.get(i).equals("UAA"))
            {
                wynik = wynik + "STOP(Ochre) ";
            }
            if (lista.get(i).equals("UAG"))
            {
                wynik = wynik + "STOP(Amber) ";
            }
            if (lista.get(i).equals("UGA"))
            {
                wynik = wynik + "STOP(Opal)";
            }
            if (lista.get(i).equals("CAU") || lista.get(i).equals("CAC"))
            {
                wynik = wynik + "histydyna ";
            }
            if (lista.get(i).equals("CAA") || lista.get(i).equals("CAG"))
            {
                wynik = wynik + "glutamina ";
            }
            if (lista.get(i).equals("AAU") || lista.get(i).equals("AAC"))
            {
                wynik = wynik + "asparagina ";
            }
            if (lista.get(i).equals("AAA") || lista.get(i).equals("AAG"))
            {
                wynik = wynik + "lizyna ";
            }
            if (lista.get(i).equals("GAU") || lista.get(i).equals("GAC"))
            {
                wynik = wynik + "asparaginian ";
            }
            if (lista.get(i).equals("GAA") || lista.get(i).equals("GAG"))
            {
                wynik = wynik + "glutaminian ";
            }
            if (lista.get(i).equals("UGG"))
            {
                wynik = wynik + "tryptofan ";
            }
            if (lista.get(i).equals("CGU") || lista.get(i).equals("CGC") || lista.get(i).equals("CGA") || lista.get(i).equals("CGG") || lista.get(i).equals("AGA") || lista.get(i).equals("AGG"))
            {
                wynik = wynik + "arginina ";
            }
            if (lista.get(i).equals("GGU") || lista.get(i).equals("GGC") || lista.get(i).equals("GGA") || lista.get(i).equals("GGG"))
            {
                wynik = wynik + "glicyna ";
            }


        }
        tvWynik.setText(kodonList +System.lineSeparator()+ System.lineSeparator() + System.lineSeparator() + wynik);
        wynik = "";
    }

    public void add()
    {
        kodonList = kodonList +" "+ etKodon.getText(); //
        lista.add(etKodon.getText().toString());
        tvWynik.setText(kodonList +System.lineSeparator()+ System.lineSeparator() + System.lineSeparator() + wynik);
        licznik++;
    }


    public void switchOption()
    {
        for (int i = 0; i < 3;i++)
        {
            A.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = 1;
                }
            });
            switch (id){
                case 1:
                    kodon = kodon + "A";
                    break;
            }
        }
        kodonList = kodonList + kodon + " ";
        kodon = "";
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void end()
    {
        translIf(lista, licznik);
    }

}
